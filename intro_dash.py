import pandas as pd
from pymongo import MongoClient
import json
import dash
import dash_table
import pandas as pd
import pymongo
from datetime import datetime, timedelta
import tzlocal

def to_group_day(d):
    if d.hour < 9:
        return d.date() - timedelta(days=1) 
    elif d.hour > 9:
        return d.date()
    else: 
        if (d.minute < 30):
            return d.date() - timedelta(days=1) 

client = pymongo.MongoClient("mongodb+srv://tuanna:tuanna123@bkluster.2bddf.mongodb.net/myFirstDatabase?retryWrites=true&w=majority")
db = client.data_raw

coll = db['LCS17']
result = list(coll.find({}))
df = pd.DataFrame(result)
# print(df)
del df['_id']
local_timezone = tzlocal.get_localzone() # get pytz timezone
df['date-local'] = df['updated_time'].apply(lambda d: datetime.fromtimestamp(d/1000, local_timezone))

df['date'] = pd.to_datetime(df['updated_time'], unit='ms')
del df['updated_time']
#
# del df['date-local']
del df['date']
del df['time']
# print(df)
df.columns = [ ' '.join(str(i) for i in col) for col in df.columns]
# df.reset_index(inplace=True)
app = dash.Dash(__name__)

app.layout = dash_table.DataTable(
    id='table',
    columns=[{"name": i, "id": i} for i in df.columns],
    data=df.to_dict('records'),
    page_size=50,
    style_table={'overflowY': 'auto'},
    fixed_rows={'headers': True},
)

if __name__ == '__main__':
    app.run_server(debug=True)