import base64
import os
from urllib.parse import quote as urlquote

from flask import Flask, send_from_directory
import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output
import pymongo
import pandas as pd
import json
import sys
from datetime import  datetime, timedelta
import tzlocal
import dash_table
import plotly.express as px


client = pymongo.MongoClient("mongodb+srv://tuanna:tuanna123@bkluster.2bddf.mongodb.net/myFirstDatabase?retryWrites=true&w=majority")
db = client.test


UPLOAD_DIRECTORY = r"C:\Users\Tuan\Desktop\doan\web//app_uploaded_files"

if not os.path.exists(UPLOAD_DIRECTORY):
    os.makedirs(UPLOAD_DIRECTORY)


# Normally, Dash creates its own Flask server internally. By creating our own,
# we can create a route for downloading files directly:
server = Flask(__name__)
app = dash.Dash(server=server, url_base_pathname='/upload/')

@server.route("/download/<path:path>")
def download(path):
    """Serve a file from the upload directory."""
    return send_from_directory(UPLOAD_DIRECTORY, path, as_attachment=True)


app.layout = html.Div(
    [
        html.H1("File Browser"),
        html.H2("Upload"),
        dcc.Upload(
            id="upload-data",
            children=html.Div(
                ["Drag and drop or click to select a file to upload."]
            ),
            style={
                "width": "100%",
                "height": "60px",
                "lineHeight": "60px",
                "borderWidth": "1px",
                "borderStyle": "dashed",
                "borderRadius": "5px",
                "textAlign": "center",
                "margin": "10px",
            },
            multiple=True,
        ),
        html.P(id='placeholder')
    ],
    style={"max-width": "500px"},
)

def mongoimport17b(csv_path, coll_name):
    """ Imports a csv file at path csv_name to a mongo colection
    returns: count of the documants in the new collection
    """
    coll = db[coll_name]
    data = pd.read_csv(csv_path)
    data['updated_time'] = pd.to_datetime(data['time'], format="%m-%d-%Y %H:%M:%S") - timedelta(hours=7)
    data.rename(columns = {'temp': 't', 'hum': 'h', 'pm25': 'pm2_5', 'aqi': 'aqi'}, inplace = True)
    payload = json.loads(data.to_json(orient='records'))
    coll.insert(payload)
    return coll.count()

def save_file(name, content):
    """Decode and store a file uploaded with Plotly Dash."""
    data = content.encode("utf8").split(b";base64,")[1]
    with open(os.path.join(UPLOAD_DIRECTORY, name), "wb") as fp:
        fp.write(base64.decodebytes(data))


def uploaded_files():
    """List the files in the upload directory."""
    files = []
    for filename in os.listdir(UPLOAD_DIRECTORY):
        path = os.path.join(UPLOAD_DIRECTORY, filename)
        if os.path.isfile(path):
            files.append(filename)
    return files


def file_download_link(filename):
    """Create a Plotly Dash 'A' element that downloads a file from the app."""
    location = "/download/{}".format(urlquote(filename))
    return html.A(filename, href=location)

@app.callback(
    Output("placeholder", "children"),
    [Input("upload-data", "filename"), Input("upload-data", "contents")],
)
def update_output(uploaded_filenames, uploaded_file_contents):
    """Save uploaded files and regenerate the file list."""

    if uploaded_filenames is not None and uploaded_file_contents is not None:
        for name, data in zip(uploaded_filenames, uploaded_file_contents):
            save_file(name, data)
            try:
                mongoimport17b(os.path.join(UPLOAD_DIRECTORY, name), 'LCS177')
                # return "Files uploaded"
            except:
                e = sys.exc_info()[0]
                return str(e)

        return "Files uploaded"
    # files = uploaded_files()
    # if len(files) == 0:
    #     return [html.Li("No files yet!")]
    # else:
    #     return [html.Li(file_download_link(filename)) for filename in files]

coll = db['LCS177']
result = list(coll.find({}))
df = pd.DataFrame(result)
# print(df)
del df['_id']
local_timezone = tzlocal.get_localzone() # get pytz timezone
df['date-local'] = df['updated_time'].apply(lambda d: datetime.fromtimestamp(d/1000, local_timezone))

df['date'] = pd.to_datetime(df['updated_time'], unit='ms')
del df['updated_time']
#
# del df['date-local']
del df['date']
del df['time']
# print(df)
df.columns = [ ''.join(str(i) for i in col) for col in df.columns]
# df.reset_index(inplace=True)


# dashapp.layout = dash_table.DataTable(
#     id='table',
#     columns=[{"name": i, "id": i} for i in df.columns],
#     data=df.to_dict('records'),
#     export_format="csv",
#     page_size=50,
#     style_table={'overflowY': 'auto'},
#     fixed_rows={'headers': True},
# )

def buil_fig():
    # print (df)
    fig = px.scatter(x= df["date-local"], y=df["temp_location1"])
    return fig

dashapp = dash.Dash(server=server, url_base_pathname='/')
dashapp.layout = html.Div(
    [
        html.H1("Main page"),
        html.A("Upload file", href='/upload/'),
        dash_table.DataTable(
            id='table',
            columns=[{"name": i, "id": i} for i in df.columns],
            data=df.to_dict('records'),
            export_format="csv",
            page_size=50,
            # row_deletable=True,
            # table style (ordered by increased precedence: see
            # https://dash.plot.ly/datatable/style in § "Styles Priority"
            # style table
            style_table={
                'maxHeight': '50ex',
                'overflowY': 'auto',
                'width': '100%',
                'minWidth': '100%',
            },
            # style cell
            style_cell={
                'fontFamily': 'Open Sans',
                'textAlign': 'center',
                'height': '60px',
                'padding': '2px 22px',
                'whiteSpace': 'inherit',
                'overflow': 'hidden',
                'textOverflow': 'ellipsis',
            },
            style_cell_conditional=[
                {
                    'if': {'column_id': 'State'},
                    'textAlign': 'left'
                },
            ],
            # style header
            style_header={
                'fontWeight': 'bold',
                'backgroundColor': 'white',
            },
            # style filter
            # style data
            style_data_conditional=[
                {
                    # stripped rows
                    'if': {'row_index': 'odd'},
                    'backgroundColor': 'rgb(248, 248, 248)'
                },
                # {
                #     # highlight one row
                #     'if': {'row_index': 4},
                #     "backgroundColor": "#3D9970",
                #     'color': 'white'
                # }
            ],
        ),
        dcc.Graph(figure=buil_fig())
    ],
)
@server.route('/upload/')
def render_dashboard():
    return Flask.redirect('/upload/')

@server.route('/')
def home():
    return Flask.redirect('/')
if __name__ == "__main__":
    # app.run_server(debug=True, port=8888)
    server.run(debug=True, port=8888)